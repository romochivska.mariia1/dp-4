﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ШПЛР4
{ /*7. Користувач здійснює планування своїх покупок. Для цього він може додавати певну кількість Товару (Назва, виробник, ціна)
   до кошика та вилучати його. Забезпечити можливість відміни декількох останніх  дій користувача з кошиком.*/
    class Product
    {
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public decimal Price { get; set; }

        public override string ToString()
        {
            return $"{Name} - {Manufacturer} - {Price:C}";
        }
    }

    class ShoppingCart
    {
        private List<Product> cartItems = new List<Product>();

        public void AddToCart(Product product)
        {
            cartItems.Add(product);
            Console.WriteLine($"Added {product} to the cart.");
        }

        public void RemoveFromCart(Product product)
        {
            if (cartItems.Contains(product))
            {
                cartItems.Remove(product);
                Console.WriteLine($"Removed {product} from the cart.");
            }
            else
            {
                Console.WriteLine($"{product} not found in the cart.");
            }
        }

        public void DisplayCart()
        {
            Console.WriteLine("Current Cart:");
            foreach (var item in cartItems)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }
    }

    interface ICommand
    {
        void Execute();
        void Undo();
    }

    class AddToCartCommand : ICommand
    {
        private readonly ShoppingCart cart;
        private readonly Product product;

        public AddToCartCommand(ShoppingCart cart, Product product)
        {
            this.cart = cart;
            this.product = product;
        }

        public void Execute()
        {
            cart.AddToCart(product);
        }

        public void Undo()
        {
            cart.RemoveFromCart(product);
        }
    }

    class RemoveFromCartCommand : ICommand
    {
        private readonly ShoppingCart cart;
        private readonly Product product;

        public RemoveFromCartCommand(ShoppingCart cart, Product product)
        {
            this.cart = cart;
            this.product = product;
        }

        public void Execute()
        {
            cart.RemoveFromCart(product);
        }

        public void Undo()
        {
            cart.AddToCart(product);
        }
    }

    class CommandInvoker
    {
        private readonly List<ICommand> commandHistory = new List<ICommand>();

        public void ExecuteCommand(ICommand command)
        {
            command.Execute();
            commandHistory.Add(command);
        }

        public void UndoLastCommands(int count)
        {
            int actionsToUndo = Math.Min(count, commandHistory.Count);

            for (int i = commandHistory.Count - 1; i >= commandHistory.Count - actionsToUndo; i--)
            {
                commandHistory[i].Undo();
                Console.WriteLine($"Undid last command: {commandHistory[i].GetType().Name}");
            }
        }
    }

    class Program
    {
        static void Main()
        {
            ShoppingCart cart = new ShoppingCart();
            CommandInvoker invoker = new CommandInvoker();

            Product product1 = new Product { Name = "Product 1", Manufacturer = "Manufacturer 1", Price = 19.99m };
            Product product2 = new Product { Name = "Product 2", Manufacturer = "Manufacturer 2", Price = 29.99m };

            ICommand addToCartCommand1 = new AddToCartCommand(cart, product1);
            ICommand addToCartCommand2 = new AddToCartCommand(cart, product2);

            invoker.ExecuteCommand(addToCartCommand1);
            invoker.ExecuteCommand(addToCartCommand2);

            cart.DisplayCart();

            invoker.UndoLastCommands(2);
            cart.DisplayCart();
        }
    }
}
